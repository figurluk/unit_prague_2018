<?php
/**
 * Created by PhpStorm.
 * User: Lukas Figura
 * Date: 21/02/2017
 * Time: 17:58
 */

Route::group(['middleware' => 'auth','admin'], function () {
	
	Route::get('/', 'Admin\AdminController@index');
	
	Route::resource('users', 'Admin\UsersController');
	
	Route::resource('answers', 'Admin\AnswerController');

	Route::get('questions', 'Admin\QuestionController@index');
    Route::get('questions/create', 'Admin\QuestionController@create');
    Route::get('questions/{id}/edit', 'Admin\QuestionController@edit');
    Route::post('questions/{id}/update', 'Admin\QuestionController@update');
    Route::post('questions', 'Admin\QuestionController@store');

    Route::get('games/{id}', 'Admin\GameController@show');

    Route::resource('options', 'Admin\OptionController');
	
	Route::resource('funfacts', 'Admin\FunfactController');

	Route::get('ranking', 'Admin\RankController@index');
	
	
});
