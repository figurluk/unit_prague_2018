<?php

Route::get('/', function () {
	return view('welcome');
});

// Authentication Routes...
$this->get('login/admin', 'Auth\LoginController@showLoginForm')->name('login');
$this->post('login/admin', 'Auth\LoginController@login');
$this->post('logout', 'Auth\LoginController@logout')->name('logout');

// Auth social
$this->get('login/facebook', 'Auth\LoginController@redirectToFacebookProvider');
$this->get('auth/login/facebook/callback', 'Auth\LoginController@handleFacebookProviderCallback');

$this->get('login/google', 'Auth\LoginController@redirectToGoogleProvider');
$this->get('auth/login/google/callback', 'Auth\LoginController@handleGoogleProviderCallback');


// Registration Routes...
$this->get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
$this->post('register', 'Auth\RegisterController@register');

// Password Reset Routes...
$this->get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
$this->post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
$this->get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
$this->post('password/reset', 'Auth\ResetPasswordController@reset');

$this->get('/home', 'HomeController@index')->name('home');

$this->get('/game', 'GameController@index')->name('game');
$this->post('/game/answer', 'GameController@storeAnswer')->name('gameAnswerStore');
$this->get('/game/video', 'GameController@video');

$this->get('/login', 'Auth\LoginController@loginClient');


// Result game Routes...
$this->get('/result/highscore', 'ResultController@highscore');
