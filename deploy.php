<?php

namespace Deployer;

require 'recipe/common.php';
require 'recipe/laravel.php';

function updateProductSeeders($seeder)
{
    $reading = fopen('database/seeds/production-seeds.php', 'r');
    $writing = fopen('database/seeds/production-seeds.tmp', 'w');

    $replaced = false;

    while (!feof($reading)) {
        $line = fgets($reading);
        if (strpos($line, $seeder)) {
            fputs($writing, $line);
            fgets($reading);
            $line = "\t\t'executed' => '" . date('d.m.Y H:i:s') . "',\n";
            $replaced = true;
        }
        fputs($writing, $line);
    }
    fclose($reading);
    fclose($writing);
    // might as well not overwrite the file if we didn't replace anything
    if ($replaced) {
        unlink('database/seeds/production-seeds.php');
        rename('database/seeds/production-seeds.tmp', 'database/seeds/production-seeds.php');
    } else {
        unlink('database/seeds/production-seeds.tmp');
    }
}

// Configuration

set('ssh_type', 'native');
set('ssh_multiplexing', true);

//set('repository', 'git@domain.com:username/repository.git');
set('repository', 'git@bitbucket.org:figurluk/unit_prague_2018.git');

// Laravel shared file
add('shared_files', []);
set('shared_dirs', []);

add('writable_dirs', [
    'storage/system',
    'config'
]);

// Servers

host('byclick.miniserver.cz')
    ->stage('production')
    ->user('admin')
    ->set('branch', 'deploy-prod')
    ->set('deploy_path', '/home/admin/www/slavokozar_sk/unit')
    ->roles('app');

// Tasks

desc('Question "Are you sure?"');
task('sure_question', function () {
    if (!askConfirmation('Are you sure to deploy on production server ?', $default = false)) {
        writeln('<comment>Aborted</comment>');
        exit(0);
    }
})->onStage(['production']);

desc('Restart PHP-FPM service');
task('php-fpm:restart', function () {
    // The user must have rights for restart service
    // /etc/sudoers: username ALL=NOPASSWD:/bin/systemctl restart php-fpm.service
    run('sudo systemctl restart php-fpm.service');
});

desc('Execute artisan migrate reset');
task('artisan:migrate:reset', function () {
    run('{{bin/php}} {{release_path}}/artisan migrate:reset --force');
})->onStage('test');


desc('Execute artisan db:seed on test');
task('artisan:db:seed-test', function () {
    run('{{bin/php}} {{release_path}}/artisan db:seed --force');
})->onStage('test');


desc('Execute artisan db:seed on production');
task('artisan:db:seed-product', function () {

    $productSeeds = require('database/seeds/production-seeds.php');

    foreach ($productSeeds as $seeder => $info) {
        if (!$info['executed']) {
            run('{{bin/php}} {{release_path}}/artisan db:seed --force --class=' . $seeder);
            updateProductSeeders($seeder);
        }
    }
})->onStage('production');


// [Optional] if deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');


desc('Execute artisan copy-env');
task('copy-env', function () {
    run('rm -f {{release_path}}/.env');
    run('yes | cp {{deploy_path}}/shared/.env {{release_path}}');
});


desc('Execute artisan config:clear');
task('artisan:config:clear', function () {
    run('{{bin/php}} {{release_path}}/artisan config:clear');
});


desc('Execute artisan route:clear');
task('artisan:route:clear', function () {
    run('{{bin/php}} {{release_path}}/artisan route:clear');
});


before('deploy:prepare', 'sure_question');

//for bad deploys fix
before('deploy:lock', 'deploy:unlock');

after('deploy:shared', 'copy-env');

// Migrate database before symlink new release.
before('deploy:symlink', 'artisan:migrate');
before('artisan:migrate', 'artisan:migrate:reset');
after('artisan:migrate', 'artisan:db:seed-test');
after('artisan:db:seed-test', 'artisan:db:seed-product');

after('artisan:config:cache', 'artisan:config:clear');
after('artisan:route:cache', 'artisan:route:clear');

after('artisan:config:cache', 'artisan:config:clear');
after('artisan:route:cache', 'artisan:route:clear');
