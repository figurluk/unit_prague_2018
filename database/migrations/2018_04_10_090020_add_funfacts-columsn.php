<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFunfactsColumsn extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('funfacts', function ( Blueprint $table ) {
			$table->string('title');
			$table->unsignedInteger('level_id');
		});
	}
	
	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('funfacts', function ( Blueprint $table ) {
			$table->dropColumn('title');
			$table->dropColumn('level_id');
		});
	}
}
