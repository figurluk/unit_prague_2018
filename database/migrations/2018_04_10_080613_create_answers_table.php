<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('answers', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('game_id');
            $table->foreign('game_id')->references('id')->on('games');
			$table->unsignedInteger('option_id');
			$table->foreign('option_id')->references('id')->on('options');
			$table->unsignedInteger('question_id');
			$table->foreign('question_id')->references('id')->on('questions');
            
            $table->float('time');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('answers', function (Blueprint $table) {
            $table->dropForeign(['game_id']);
            $table->dropForeign(['option_id']);
        });

        Schema::dropIfExists('answers');
    }
}
