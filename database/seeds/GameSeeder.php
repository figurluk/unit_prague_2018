<?php

use Illuminate\Database\Seeder;

class GameSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (range(1, rand(3, 6)) as $index) {
            $game = \App\Models\Game::create([
                'user_id' => \App\Models\User::all()->random()->id,
                'token'   => uniqid()
            ]);

            $questions = \App\Models\Question::all()->random(4);
            foreach ($questions as $question) {
                $option = $question->options->random();
                \App\Models\Answer::create([
                    'game_id'     => $game->id,
                    'option_id'   => $option->id,
                    'question_id' => $question->id,
                    'time'        => rand(5, 35)
                ]);
            }
        }
    }
}
