<?php

use Illuminate\Database\Seeder;

class QuizSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$lvl1 = \App\Models\Level::orderBy('id')->first();
		$lvl2 = \App\Models\Level::orderBy('id')->offset(1)->first();
		$lvl3 = \App\Models\Level::orderBy('id')->offset(2)->first();
		$lvl4 = \App\Models\Level::orderBy('id')->offset(3)->first();
		$lvl5 = \App\Models\Level::orderBy('id')->offset(4)->first();
		
		$this->level1($lvl1);
		
		$this->level2($lvl2);
		
		$this->level3($lvl3);
		
		$this->level4($lvl4);
		
		$this->level5($lvl5);
		
		$this->funFacts($lvl1, $lvl2, $lvl3, $lvl4, $lvl5);
	}
	
	/**
	 * @param $lvl1
	 */
	private function level1( $lvl1 ): void
	{
		$q = App\Models\Question::create([
			'content'  => 'Z jakého materiálu jsou vyrobeny auta značky Škoda?',
			'level_id' => $lvl1->id,
		]);
		
		\App\Models\Option::create([
			'question_id' => $q->id,
			'content'     => 'Guma',
			'correct'     => false,
		]);
		
		\App\Models\Option::create([
			'question_id' => $q->id,
			'content'     => 'Železo',
			'correct'     => true,
		]);
		
		\App\Models\Option::create([
			'question_id' => $q->id,
			'content'     => 'Papier',
			'correct'     => false,
		]);
		
		$q = \App\Models\Question::create([
			'content'  => 'Kde se lisují díly na auta značky Škoda?',
			'level_id' => $lvl1->id,
		]);
		
		\App\Models\Option::create([
			'question_id' => $q->id,
			'content'     => 'Praha',
			'correct'     => false,
		]);
		
		\App\Models\Option::create([
			'question_id' => $q->id,
			'content'     => 'Brno',
			'correct'     => false,
		]);
		
		\App\Models\Option::create([
			'question_id' => $q->id,
			'content'     => 'Mladá Boleslav',
			'correct'     => true,
		]);
	}
	
	/**
	 * @param $lvl2
	 */
	private function level2( $lvl2 ): void
	{
		$q = App\Models\Question::create([
			'content'  => 'Jak se spojují karosérni díly aut značky Škoda?',
			'level_id' => $lvl2->id,
		]);
		
		\App\Models\Option::create([
			'question_id' => $q->id,
			'content'     => 'Lepením',
			'correct'     => false,
		]);
		
		\App\Models\Option::create([
			'question_id' => $q->id,
			'content'     => 'Svařováním',
			'correct'     => true,
		]);
		
		\App\Models\Option::create([
			'question_id' => $q->id,
			'content'     => 'Lepíci páskou',
			'correct'     => false,
		]);
		
		$q = \App\Models\Question::create([
			'content'  => 'Kdo svařuje díly aut značky Škoda?',
			'level_id' => $lvl2->id,
		]);
		
		\App\Models\Option::create([
			'question_id' => $q->id,
			'content'     => 'Roboti',
			'correct'     => true,
		]);
		
		\App\Models\Option::create([
			'question_id' => $q->id,
			'content'     => 'Pracovníci společnosti Škoda',
			'correct'     => false,
		]);
		
		\App\Models\Option::create([
			'question_id' => $q->id,
			'content'     => 'Ježíšek',
			'correct'     => false,
		]);
	}
	
	/**
	 * @param $lvl4
	 */
	private function level4( $lvl4 ): void
	{
		$q = App\Models\Question::create([
			'content'  => 'Kolik kol mají auta značky Škoda?',
			'level_id' => $lvl4->id,
		]);
		
		\App\Models\Option::create([
			'question_id' => $q->id,
			'content'     => '1',
			'correct'     => false,
		]);
		
		\App\Models\Option::create([
			'question_id' => $q->id,
			'content'     => '2',
			'correct'     => false,
		]);
		
		\App\Models\Option::create([
			'question_id' => $q->id,
			'content'     => '4',
			'correct'     => true,
		]);
	}
	
	/**
	 * @param $lvl5
	 */
	private function level5( $lvl5 ): void
	{
		$q = App\Models\Question::create([
			'content'  => 'Auta značky škoda ?',
			'level_id' => $lvl5->id,
		]);
		
		\App\Models\Option::create([
			'question_id' => $q->id,
			'content'     => 'Plavou',
			'correct'     => false,
		]);
		
		\App\Models\Option::create([
			'question_id' => $q->id,
			'content'     => 'Létají',
			'correct'     => false,
		]);
		
		\App\Models\Option::create([
			'question_id' => $q->id,
			'content'     => 'Jezdí po cestách',
			'correct'     => true,
		]);
	}
	
	/**
	 * @param $lvl3
	 */
	private function level3( $lvl3 ): void
	{
		$q = App\Models\Question::create([
			'content'  => 'Jak se barví auta značky Škoda?',
			'level_id' => $lvl3->id,
		]);
		
		\App\Models\Option::create([
			'question_id' => $q->id,
			'content'     => 'Barvičkami',
			'correct'     => false,
		]);
		
		\App\Models\Option::create([
			'question_id' => $q->id,
			'content'     => 'Štětci',
			'correct'     => false,
		]);
		
		\App\Models\Option::create([
			'question_id' => $q->id,
			'content'     => 'Spreji',
			'correct'     => true,
		]);
	}
	
	/**
	 * @param $lvl1
	 * @param $lvl2
	 * @param $lvl3
	 * @param $lvl4
	 * @param $lvl5
	 */
	private function funFacts( $lvl1, $lvl2, $lvl3, $lvl4, $lvl5 ): void
	{
		\App\Models\Funfact::create([
			'level_id' => $lvl1->id,
			'title'    => 'Lisování pro celou Evropu',
			'content'  => 'Karoserie díly značky Škoda se pro celou Evropu lisují v Mladé Boleslav.',
		]);
		
		\App\Models\Funfact::create([
			'level_id' => $lvl2->id,
			'title'    => 'Cena svařovacího robota',
			'content'  => 'Cena svařovacího robota je tak vysoká, že na každý díl karoserie značky Škoda existuje jen jeden exemplář.',
		]);
		
		\App\Models\Funfact::create([
			'level_id' => $lvl3->id,
			'title'    => 'Kvalita laku aut značky Škoda',
			'content'  => 'Auta značky Škoda jsou stříkané kvalitními laky v lakovně v níž není kyslík.',
		]);
		
		\App\Models\Funfact::create([
			'level_id' => $lvl4->id,
			'title'    => 'Spojení podvozku s karoserií',
			'content'  => 'Spojení podvozku s karoserií se ve společnosti Škoda nazývá "svatba".',
		]);
		
		\App\Models\Funfact::create([
			'level_id' => $lvl5->id,
			'title'    => 'Testovací okruh Škoda',
			'content'  => 'Každé auto značky Škoda na konci výroby projde testovacím okruhem.',
		]);
	}
}
