<?php

use App\Models\Level;
use Illuminate\Database\Seeder;

class LevelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $names = [
            'Lisovna',
            'Svařování',
            'Lakovna',
            'Montáž',
            'Testování'
        ];

        collect($names)->each(function ($name) {
            Level::create([
                'name' => $name
            ]);
        });
    }
}
