<?php

return [
	
	/*
	|--------------------------------------------------------------------------
	| Third Party Services
	|--------------------------------------------------------------------------
	|
	| This file is for storing the credentials for third party services such
	| as Stripe, Mailgun, SparkPost and others. This file provides a sane
	| default location for this type of information, allowing packages
	| to have a conventional place to find your various credentials.
	|
	*/
	
	'mailgun' => [
		'domain' => env('MAILGUN_DOMAIN'),
		'secret' => env('MAILGUN_SECRET'),
	],
	
	'ses' => [
		'key'    => env('SES_KEY'),
		'secret' => env('SES_SECRET'),
		'region' => 'us-east-1',
	],
	
	'sparkpost' => [
		'secret' => env('SPARKPOST_SECRET'),
	],
	
	'stripe' => [
		'model'  => App\Models\User::class,
		'key'    => env('STRIPE_KEY'),
		'secret' => env('STRIPE_SECRET'),
	],
	
	'facebook' => [
		'client_id'     => '576193549236772',
		'client_secret' => '547c02e71d0e1e7ad9a75d6eab02276a',
		'redirect'      => 'http://localhost/auth/login/facebook/callback',
	],
	
	'google' => [
		'client_id'     => '1093909342687-cfj6e4a3ifgj5etmrhmelp4l7v33k9he.apps.googleusercontent.com',
		'client_secret' => 'faji7KsUjcCOuwpC6hMCIHjC',
		'redirect'      => 'http://localhost/auth/login/google/callback',
	],

];
