<?php

return [
    'date' => [
        'db' => 'Y-m-d',
        'web' => 'j.n.Y',
        'invoice' => 'j.n.Y',
        'html' => [
            'datetime' => 'Y-m-d\TH:i:s'
        ]
    ],

    'timestamp' => [
        'db' => 'Y-m-d H:i:s',
        'web' => 'j.n.Y H:i:s',
        'invoice' => 'j.n.Y H:i',
        'html' => [
            'datetime' => 'Y-m-d\TH:i:s'
        ]
    ]
];