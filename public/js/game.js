$(function () {
    var $window = $(window);
    var $game = $('#game');
    var $map = $('#map');
    var $results = $('#results');
    var $mapLevels = $('#map-levels');


    var $player = $('#player');

    var playerStatus = 'standing-right';
    var playerTimer = null;

    var activeLevel = 1;
    $player.css({left: -100 + 'px'});

    setTimeout(function () {
        $player
            .removeClass('standing-right').addClass('running-right')
            .animate({left: (window.innerWidth / 2 - 50) + 'px'}, 2000, function () {
                // $(this).removeClass('running-right').addClass('standing-right');

                $mapLevels.find('a').first().removeClass('disabled');
                $player.animate({left: '100vw'}, 2000, function () {
                    startLevel(1);
                })

            });


    }, 5000);


    $mapLevels.find('a').click(function (e) {
        e.stopPropagation();
        e.preventDefault();
        var $link = $(this);
        if ($link.hasClass('disabled')) {
            return;
        }

        var level = $link.attr('href').replace('#', '');

        startLevel(level);
    });


    window.scrollTo(0, 0);
    var oldScroll = 0;

    $window.scroll(function () {
        var scroll = $window.scrollTop();

        $('.level.active .content').css({left: '-' + scroll + 'px'});

        if (oldScroll > scroll) {

            playerRunningLeft();

        } else if (oldScroll < scroll) {

            playerRunningRight();

        }

        oldScroll = scroll;
    });

    function playerStandingRight() {
        if (playerStatus != 'standing-right') {
            $player.removeClass(playerStatus).addClass('standing-right');
        }

        playerTimer = null;

        playerStatus = 'standing-right';
    }

    function playerStandingLeft() {
        if (playerStatus != 'standing-left') {
            $player.removeClass(playerStatus).addClass('standing-left');
        }

        playerTimer = null;

        playerStatus = 'standing-left';
    }

    function playerRunningRight() {
        if (playerStatus != 'running-right') {
            $player.removeClass(playerStatus).addClass('running-right');
        }

        if (playerTimer != null) clearTimeout(playerTimer);
        playerTimer = setTimeout(playerStandingRight, 200);

        playerStatus = 'running-right';
    }

    function playerRunningLeft() {
        if (playerStatus != 'running-left') {
            $player.removeClass(playerStatus).addClass('running-left');
        }

        if (playerTimer != null) clearTimeout(playerTimer);
        playerTimer = setTimeout(playerStandingRight, 200);

        playerStatus = 'running-left';
    }


    var testStart = null;

    $(document).on('click', '.level.active .btn-default', function (e) {
        e.preventDefault();
        e.stopPropagation();

        testStart = Date.now();

        $(this).addClass('hidden').closest('.level').find('.test').removeClass('hidden');
    });

    $(document).on('click', '.level.active .btn-primary', function (e) {
        e.stopPropagation();
        e.preventDefault();

        var time = Date.now() - testStart;
        var data = $('.level.active form').serializeArray().reduce(function (obj, item) {
            obj[item.name] = item.value;
            return obj;
        }, {});

        endLevel();

		saveAnswer(activeLevel, {
			question_id: data.question_id,
			time       : time,
			option_id  : data.option1
		});

		submitAnswer(activeLevel);

	});


    function startLevel(level) {
        window.scrollTo(0,0);

        activeLevel = level;
        $player
            .removeClass('standing-right')
            .addClass('running-right')
            .animate({left: '100vw'}, 2000, function () {
                $map.addClass('hidden');

                var $level = $('#level' + level);
                $level.addClass('active');
                $player
                    .addClass('running-right')
                    .css({left: -100 + 'px'})

                    .animate({left: (window.innerWidth / 2 - 50) + 'px'}, 2000, function () {
                        $(this).removeClass('running-right').addClass('standing-right');

                        var width = $('.level.active .content').outerWidth();
                        $('body').css({height: (width + window.innerHeight - window.innerWidth) + 'px'});

                    });


            });


    }

    function endLevel() {

        $('#player').addClass('running-right').animate({left: 100 + 'vw'}, 2000, function () {
            $('.level.active').removeClass('active');


            $map.removeClass('hidden');

            $player
                .addClass('running-right')
                .css({left: -100 + 'px'})
                .animate({left: (window.innerWidth / 2 - 50)}, 2000, function () {
                    var $activeLink = $mapLevels.find('a').not('.disabled');
                    $activeLink.addClass('disabled');
                    if ($activeLink.next().length > 0) {
                        $activeLink.next().removeClass('disabled');
                        startLevel(activeLevel + 1);
                    } else {
                        showResults();
                    }
                });


        });

    }


    function showResults() {

        $('#player').addClass('running-right').animate({left: 100 + 'vw'}, 2000, function () {

            $map.addClass('hidden');
            $('#results').removeClass('hidden');

            $.ajax({
                url: '/result/highscore',
                method: 'get'
            }).done(function (data) {
                $('#results #loader').replaceWith(data);
            })

        })

    }

});

