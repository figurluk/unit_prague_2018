function saveAnswer(forLevel, jsonAnswer) {
	console.log(forLevel, jsonAnswer);
	storeItem(forLevel, jsonAnswer)
}

function submitAnswer(forLevel) {
	var answer = getItem(forLevel)

	console.log(answer);

	$.ajax({
		method : 'POST',
		url    : '/game/answer',
		data   : {
			question_id: answer.question_id,
			option_id  : answer.option_id,
			time       : answer.time
		},
		success: function (data) {
			deleteItem(forLevel)
		},
		error  : function (msg) {

		}
	});
}

function storeAnswers() {
	for (var key in localStorage) {
		submitAnswer(key);
	}
}
