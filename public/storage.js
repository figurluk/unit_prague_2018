function storeItem(key, value) {
	localStorage.setItem(key, JSON.stringify(value));
}

function getItem(key) {
	return JSON.parse(localStorage.getItem(key));
}

function deleteItem(key) {
	localStorage.removeItem(key)
}
