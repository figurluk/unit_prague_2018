<?php

namespace App\Models;

use App\Models\Level;
use App\Models\Option;
use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $table = 'questions';

    protected $fillable = [
    	'level_id',
        'content'
    ];

    public function level()
    {
        return $this->belongsTo(Level::class, 'level_id');
    }

    public function options()
    {
        return $this->hasMany(Option::class, 'question_id');
    }
}
