<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    protected $table = 'answers';

    protected $fillable = [
        'game_id',
        'option_id',
		'question_id',
		'time'
    ];

    public function getCorrectAttribute()
    {
        return $this->option->correct;
    }

    public function game()
    {
        return $this->belongsTo(Game::class, 'game_id');
    }
	
	public function option()
	{
		return $this->belongsTo(Option::class, 'option_id');
	}
	
	public function question()
	{
		return $this->belongsTo(Question::class, 'question_id');
	}
}
