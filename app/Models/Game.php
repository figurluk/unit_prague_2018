<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Game extends Model
{
    protected $table = 'games';

    protected $fillable = [
        'user_id',
        'token',
    ];

    public function getTimeAttribute()
    {
        return $this->answers()->sum('time');
    }

    public function getPointsAttribute()
    {
        return $this->answers->filter(function ($answer) {
            return $answer->correct;
        })->count();
    }

    public function getRankAttribute()
    {
        $gamesSorted = Game::all()
            ->sortByDesc(function ($game) {
                return $game->points;
            });

        foreach ($gamesSorted as $index => $game) {
            if ($game->id == $this->id) {
                return $index + 1;
            }
        }

        return 1;
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function answers()
    {
        return $this->hasMany(Answer::class);
    }

    public function completed()
    {
        foreach (Level::get() as $level) {
            if (!$this->answers()->whereHas('question', function ($q) use ($level) {
                $q->where('level_id', $level->id);
            })->exists()) return false;
        }

        return true;
    }

    public function progress()
    {
        $percent = 0;
        $percentPart = 100 / Level::get()->count();
        foreach (Level::orderBy('id', 'asc')->get() as $level) {
            if (!$this->answers()->whereHas('question', function ($q) use ($level) {
                $q->where('level_id', $level->id);
            })->exists()) break;

            $percent += $percentPart;
        }

        return $percent;
    }

    public function scopeToday($query)
    {
        return $query->where('created_at', '>=', Carbon::today());
    }
}
