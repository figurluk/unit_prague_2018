<?php

namespace App\Models;

use App\Models\Question;
use Illuminate\Database\Eloquent\Model;

class Level extends Model
{
    protected $table = 'levels';

    protected $fillable = [
        'name'
    ];

    public function questions()
    {
        return $this->hasMany(Question::class, 'level_id');
    }

    public function funfacts()
    {
        return $this->hasMany(Funfact::class, 'level_id');
    }
}
