<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Funfact extends Model
{
	use SoftDeletes;
	
    protected $table = 'funfacts';
    
    protected $fillable = [
        'level_id',
        'content',
		'title'
    ];

    public function level()
    {
        return $this->belongsTo(Level::class, 'level_id');
    }
}
