<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;

class LoginController extends Controller
{
	/*
	|--------------------------------------------------------------------------
	| Login Controller
	|--------------------------------------------------------------------------
	|
	| This controller handles authenticating users for the application and
	| redirecting them to your home screen. The controller uses a trait
	| to conveniently provide its functionality to your applications.
	|
	*/
	
	use AuthenticatesUsers;
	
	/**
	 * Where to redirect users after login.
	 *
	 * @var string
	 */
	protected $redirectTo = '/';
	
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('guest')->except('logout');
	}
	
	public function loginClient()
	{
		return view('homepage.login-client');
	}
	
	/**
	 * Redirect the user to the GitHub authentication page.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function redirectToFacebookProvider()
	{
		return Socialite::driver('facebook')->redirect();
	}
	
	/**
	 * Obtain the user information from GitHub.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function handleFacebookProviderCallback()
	{
		$fbUser = Socialite::driver('facebook')->user();

//        check if user is in system
		$systemUser = User::where('facebook_id', $fbUser->id)->first();
		if ($systemUser == null && isset($fbUser->email)) {
			$systemUser = User::where('email', $fbUser->email)->first();
			if ($systemUser != null) {
				$systemUser->facebook_id = $fbUser->id;
				$systemUser->save();
			}
		}
		
		if ($systemUser == null) {
			$systemUser = User::create([
				'name'              => $fbUser->name,
				'email'             => isset($fbUser->email) ? $fbUser->email : null,
				'password'          => bcrypt(uniqid()),
				'facebook_id'       => $fbUser->id,
			]);
		}
		
		Auth::login($systemUser);
		
		return redirect()->action('GameController@index');
	}
	
	/**
	 * Redirect the user to the GitHub authentication page.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function redirectToGoogleProvider()
	{
		return Socialite::driver('google')->redirect();
	}
	
	/**
	 * Obtain the user information from GitHub.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function handleGoogleProviderCallback()
	{
		$googleUser = Socialite::driver('google')->user();
		
		$systemUser = User::where('google_id', $googleUser->id)->first();
		if ($systemUser == null && isset($googleUser->email)) {
			$systemUser = User::where('email', $googleUser->email)->first();
			if ($systemUser != null) {
				$systemUser->google_id = $googleUser->id;
				$systemUser->save();
			}
		}
		if ($systemUser == null) {
			$systemUser = User::create([
				'name'              => $googleUser->name,
				'email'             => isset($googleUser->email) ? $googleUser->email : null,
				'password'          => bcrypt(uniqid()),
				'google_id'         => $googleUser->id,
			]);
		}
		
		Auth::login($systemUser);
		
		return redirect()->action('GameController@index');
	}
}
