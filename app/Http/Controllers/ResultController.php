<?php

namespace App\Http\Controllers;

use App\Models\Game;

class ResultController extends Controller
{
    /**
     * @return \Illuminate\Http\Response
     */
    public function highscore()
    {
        $gamesSorted = Game::all()
            ->sortByDesc('points');

        return view('result.highscore', compact('gamesSorted'));
    }
}
