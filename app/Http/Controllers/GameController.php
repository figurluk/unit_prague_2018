<?php

namespace App\Http\Controllers;

use App\Models\Answer;
use App\Models\Game;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class GameController extends Controller
{
	
	/**
	 * GameController constructor.
	 */
	public function __construct()
	{
		$this->middleware(function ($request, Closure $next) {
			if (!Auth::check()) {
				return redirect()->action('Auth\LoginController@loginClient');
			} else {
				return $next($request);
			}
		});
	}
	
	public function index()
	{
//		if (!Session::has('game_token')) {
			
			$game = Game::create([
				'user_id' => Auth::id(),
				'token'   => uniqid(),
			]);
			
			Session::put('game_token', $game->token);
//		} else {
//			$resume = true;
//			$game = Game::where('token', Session::get('game_token'))->first();
//
//			if ($game->completed()) {
//
//			} else {
//
//			}
//
//		}
		
		return view('game.index');
	}
	
	public function storeAnswer( Request $request )
	{
		$this->validate($request, [
			'question_id' => 'required|exists:questions,id',
			'option_id'   => 'required|exists:options,id',
			'time'        => 'required',
		]);
		
		$game = Game::where('token', Session::get('game_token'))->first();
		
		$answer = Answer::create([
			'user_id'     => Auth::id(),
			'option_id'   => $request->option_id,
			'question_id' => $request->question_id,
			'game_id'     => $game->id,
			'time'        => number_format($request->time / 1000,3,'.',''),
		]);
		
		return response('ok');
	}

    public function video()
    {
        return view('game.video');
	}
}
