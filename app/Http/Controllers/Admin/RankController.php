<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Game;

class RankController extends Controller
{
    public function index()
    {
        $gamesSorted = Game::all()
            ->sortByDesc(function($game) {
                return $game->points;
            });

        return view('admin.questions.rank.index', compact('gamesSorted'));
    }
}
