<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Level;
use App\Models\Option;
use App\Models\Question;
use Illuminate\Http\Request;

class QuestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $questions = Question::paginate(10);
        return view('admin.questions.index', compact('questions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $levels = Level::all();
        return view('admin.questions.create', compact('levels'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'level'          => 'required|exists:levels,id',
            'content'        => 'required|string',
            'option1'        => 'required|string',
            'option2'        => 'required|string',
            'option3'        => 'required|string',
            'correct_option' => 'required|numeric'
        ]);

        $question = Question::create([
            'level_id' => $request->level,
            'content'  => $request->content
        ]);

        foreach (range(1, 3) as $index) {
            Option::create([
                'question_id' => $question->id,
                'content'     => $request->{'option' . $index},
                'correct'     => $request->correct_option == $index,
            ]);
        }

        flash('Question created');
        return redirect()->action('Admin\QuestionController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $question = Question::findOrFail($id);
        $levels = Level::all();

        return view('admin.questions.edit', compact('question', 'levels'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'level'   => 'required|exists:levels,id',
            'content' => 'required|string',
            'option1' => 'required|string',
            'option2' => 'required|string',
            'option3' => 'required|string'
        ]);

        $question = Question::findOrFail($id);

        $question->update([
            'level_id' => $request->level,
            'content'  => $request->content
        ]);

        foreach ($question->options as $index => $option) {
            $option->update([
                'question_id' => $question->id,
                'content'     => $request->{'option' . ($index + 1)},
                'correct'     => $request->correct_option == ($index + 1),
            ]);
        }

        return redirect()->action('Admin\QuestionController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    }
}
