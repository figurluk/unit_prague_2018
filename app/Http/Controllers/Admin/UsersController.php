<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateUserRequest;
use App\Models\Game;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UsersController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::orderBy('created_at', 'desc')->paginate(10);

        return view('admin.users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.users.create');
    }

    public function show($id)
    {
        $user = User::findOrFail($id);

        return view('admin.users.show', compact('user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateUserRequest|Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(CreateUserRequest $request)
    {
        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password)
        ]);

        flash()->success('Úspešne ste vytvorili užívateľa: ' . $user->name . ' ' . $user->surname);
        return redirect()->action('Admin\UsersController@edit', $user->id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);

        return view('admin.users.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param CreateUserRequest|Request $request
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);
        $this->validate($request, [
            'name' => 'required|max:255|string'
        ]);

        if ($request->email != $request->old_email) {
            $this->validate($request, [
                'email' => 'required|email|max:255|unique:users,email,NULL,id,deleted_at,NULL']);
        }

        $user->update($request->only('name', 'email'));

        if ($request->input('password') != null) {
            $this->validate($request, ['password' => 'required|min:6|confirmed'],
                ['password.required' => 'Password is required']);
            $password = $request->input('password');
            $hashedPass = Hash::make($password);
            $user->update(['password' => $hashedPass]);
        }

        flash()->success('Úspešne ste upravili užívateľa: ' . $user->name . ' ' . $user->surname);

        return redirect()->action('Admin\UsersController@edit', $user->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $userName = $user->name . " " . $user->surname;
        $user->delete();
        flash()->success('Úspešne ste zmazali užívateľa: ' . $userName);

        return redirect()->action('Admin\UsersController@index');
    }
}
