<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Game;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Storage;
use League\Flysystem\File;

class AdminController extends Controller
{
	
	public function index()
	{
	    $gamesToday = Game::today()->count();
	    $avgCorrectAnswers = Game::all()->pluck('rank')->average();
		return view('admin.main.home', compact('gamesToday', 'avgCorrectAnswers'));
	}
	
}
