<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Funfact;
use Illuminate\Http\Request;

class FunfactController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$funfacts = Funfact::orderBy('created_at', 'desc')->paginate(15);
		
		return view('admin.funfacts.index', compact('funfacts'));
	}
	
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		return view('admin.funfacts.create');
	}
	
	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function store( Request $request )
	{
		$this->validate($request, [
			'title'    => 'required|string|max:255',
			'content'  => 'required',
			'level_id' => 'required',
		]);
		
		Funfact::create($request->only(['title', 'content', 'level_id']));
		
		flash()->success('Successfully created Fun fact');
		
		return redirect()->action('Admin\FunfactController@index');
	}
	
	/**
	 * Display the specified resource.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function show( $id )
	{
		//
	}
	
	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function edit( $id )
	{
		$funfact = Funfact::findOrFail($id);
		
		return view('admin.funfacts.edit',compact('funfact'));
	}
	
	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  int                      $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function update( Request $request, $id )
	{
		$funfact = Funfact::findOrFail($id);
		
		$this->validate($request, [
			'title'    => 'required|string|max:255',
			'content'  => 'required',
			'level_id' => 'required',
		]);
		
		$funfact->update($request->only(['title', 'content', 'level_id']));
		
		flash()->success('Successfully updated Fun fact');
		
		return redirect()->action('Admin\FunfactController@index');
	}
	
	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function destroy( $id )
	{
		$funfact = Funfact::findOrFail($id);
		
		$funfact->delete();
		
		flash()->success('Successfully deleted Fun fact');
		
		return redirect()->action('Admin\FunfactController@index');
	}
}
