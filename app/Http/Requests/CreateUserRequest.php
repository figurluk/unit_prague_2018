<?php

namespace App\Http\Requests;


class CreateUserRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'             => 'required|max:255|string',
            'email'            => 'required|email|max:255|unique:users,email,NULL,id,deleted_at,NULL',
            'password'         => 'required|confirmed|min:6',
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'name.required'      => 'Meno musi byt zadane.',
            'surname.required'   => 'Priezvisko musi byt zadane.',
            'email.required'     => 'Email musi byt zadany.',
            'password.required'  => 'Heslo musi byt zadane.',
            'name.string'        => 'Meno musi byt postupnost znakov.',
            'surname.string'     => 'Priezvisko musi byt postupnost znakov.',
            'name.max'           => 'Meno moze obsahovat maximalne 255 znakov.',
            'surname.max'        => 'Priezvisko moze obsahovat maximalne 255 znakov.',
            'email.max'          => 'Email moze obsahovat maximalne 255 znakov.',
            'password.min'       => 'Heslo musi obsahovat minimalne 6 znakov.',
            'password.confirmed' => 'Hesla sa musia zhodovat.',
            'email.email'        => 'Email musi byt platna emailova adresa.',
            'email.unique'       => 'Zadany email uz je zaregistrovany.',
        ];
    }
}
