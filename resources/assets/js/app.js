/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

$('#email-login').click(function (ev) {
	ev.preventDefault();

	$('#email-login-form').toggleClass('hidden');
});
