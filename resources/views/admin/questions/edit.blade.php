@extends('layouts.admin')

@section('content')

    <form method="post" action="{{ action('Admin\QuestionController@update', $question->id) }}">

        @include('admin.partials.formButtons')

        @csrf

        <div class="row">
            <div class="col-md-6">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">
                            Edit question
                        </h3>
                    </div>

                    <div class="box-body">
                        <div class="form-group">
                            <label for="level">Level</label>
                            <select id="level" name="level" class="form-control">
                                @foreach($levels as $level)
                                    <option {{ $level->id == $question->level_id ? 'selected' : '' }} value="{{ $level->id }}">{{ $level->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="content">Content</label>
                            <textarea required id="content" class="form-control" name="content">
                                {{ old('content', $question->content) }}
                            </textarea>
                        </div>
                    </div>
                    <div class="box-footer">

                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">
                            Options
                        </h3>
                    </div>

                    <div class="box-body">
                        @foreach($question->options as $option)
                            <div class="form-group">
                                <label>#{{ $loop->iteration }} option</label>
                                <input class="form-control" value="{{ old('option' . $loop->iteration, $option->content) }}"
                                       name="option{{ $loop->iteration }}" type="text">
                            </div>
                            <div class="form-group">
                                <label>Correct</label>
                                <input required value="{{ $loop->iteration }}" {{ $option->correct ? 'checked' : '' }}
                                       name="correct_option" type="radio">
                            </div>
                        @endforeach
                    </div>
                    <div class="box-footer">

                    </div>
                </div>
            </div>

        </div>
    </form>

@endsection

@section('scripts')
@endsection
