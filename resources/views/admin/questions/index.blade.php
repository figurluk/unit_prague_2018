@extends('layouts.admin')

@section('content')

    <div class="row">
        <div class="col-lg-12">
            <div class="box">
                <div class="box-header simple">
                    Questions
                </div>
                <div class="box-body">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>
                                <p>Level</p>
                            </th>
                            <th>Content</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td colspan="2">
                                New question:
                            </td>
                            <td class="table-btn-column">
                                <a href="{{action('Admin\QuestionController@create')}}"
                                   class="btn btn-sm btn-default"><i class="fa fa-plus"></i> Create</a>
                            </td>
                        </tr>
                        @foreach($questions as $question)
                            <tr>
                                <td>
                                    {{ $question->level->name }}
                                </td>
                                <td>
                                    {{ $question->content }}
                                </td>
                                <td class="table-btn-column">

                                    <a class="btn btn-sm btn-default"
                                       href="{{action('Admin\QuestionController@edit', $question->id)}}">
                                        <i class="fa fa-wrench"></i> Edit
                                    </a>

                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                    {!! $questions->render() !!}

                </div>
            </div>
        </div>

    </div>

@endsection

@section('scripts')
@endsection
