@extends('layouts.admin')

@section('content')


    <div class="row">
        <div class="col-lg-12">
            <div class="box">
                <div class="box-header simple">
                    Ranking
                </div>
                <div class="box-body">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>User</th>
                            <th>Points</th>
                            <th>Time</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($gamesSorted as $game)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>
                                    {{$game->user->name}}
                                </td>
                                <td>{{ $game->points }}</td>
                                <td>{{ $game->time }}</td>
                                <td class="table-btn-column">

                                    <a href="{{ action('Admin\GameController@show', $game->id) }}"
                                       class="btn btn-sm btn-primary">
                                        Detail
                                    </a>

                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </div>

    </div>
@endsection