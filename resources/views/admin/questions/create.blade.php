@extends('layouts.admin')

@section('content')

    <form method="post" action="{{ action('Admin\QuestionController@store') }}">

        @include('admin.partials.formButtons')

        @csrf

        <div class="row">
            <div class="col-md-6">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">
                            New question
                        </h3>
                    </div>

                    <div class="box-body">
                        <div class="form-group">
                            <label for="level">Level</label>
                            <select required id="level" name="level" class="form-control">
                                @foreach($levels as $level)
                                    <option {{ $level->id == old('level') ? 'selected' : '' }} value="{{ $level->id }}">{{ $level->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="content">Content</label>
                            <textarea required id="content" class="form-control"
                                      name="content">{{ old('content') }}</textarea>
                        </div>
                    </div>
                    <div class="box-footer">

                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">
                            Options
                        </h3>
                    </div>

                    <div class="box-body">
                        @foreach(range(1,3) as $index)
                            <div class="form-group">
                                <label>#{{ $index }} option</label>
                                <input required class="form-control" value="{{ old('option' . $index) }}"
                                       name="option{{ $index }}" type="text">
                            </div>
                            <div class="form-group">
                                <label>Correct</label>
                                <input required value="{{ $index }}"
                                       name="correct_option" type="radio">
                            </div>
                        @endforeach
                    </div>
                    <div class="box-footer">

                    </div>
                </div>
            </div>
        </div>
    </form>

@endsection

@section('scripts')
@endsection
