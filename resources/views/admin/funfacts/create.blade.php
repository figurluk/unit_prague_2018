@extends('layouts.admin')


@section('content')
	{!! Form::open(['action'=>'Admin\FunfactController@store', 'method'=>'POST','class'=>'form-horizontal']) !!}
	
	
	@include('admin.partials.formButtons')
	
	
	<div class="row">
		@include('admin.partials.formErrors')
		
		<div class="col-lg-7">
			<div class="box">
				<div class="box-header simple">
					Settings
				</div>
				<div class="box-body">
					<div class="form-group">
						{!! Form::label('title', 'Title:', ['class'=>'col-sm-4 control-label']) !!}
						<div class="col-sm-8">
							{!! Form::text('title', old('title'), ['class'=>'form-control','placeholder'=>'Title...']) !!}
						</div>
					</div>
					<div class="form-group">
						{!! Form::label('level_id', 'Level:', ['class'=>'col-sm-4 control-label']) !!}
						<div class="col-sm-8">
							<select name="level_id" id="level_id" class="form-control">
								@foreach(\App\Models\Level::get() as $level)
									<option value="{{$level->id}}" {{old('level_id')!=$level->id ? :'selected'}}>{{$level->name}}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="form-group">
						{!! Form::label('content', 'Content:', ['class'=>'col-sm-4 control-label']) !!}
						<div class="col-sm-8">
							{!! Form::textarea('content', old('content'), ['class'=>'form-control','placeholder'=>'Content...']) !!}
						</div>
					</div>
				</div>
			</div>
		
		</div>
	
	
	</div>
	{!! Form::close() !!}
@endsection

@section('scripts')

@endsection
