@extends('layouts.admin')

@section('content')
	
	
	<div class="row">
		<div class="col-lg-12">
			<div class="box">
				<div class="box-header simple">
					All funfacts
				</div>
				<div class="box-body">
					<table class="table table-striped table-bordered table-hover">
						<thead>
						<tr>
							<th>
								Title
							</th>
							<th>
								Level
							</th>
							<th></th>
						</tr>
						</thead>
						<tbody>
						<tr>
							<td colspan="2">
								New funfact:
							</td>
							<td class="table-btn-column">
								<a href="{{action('Admin\FunfactController@create')}}"
								   class="btn btn-sm btn-default"><i class="fa fa-plus"></i> Create</a>
							</td>
						</tr>
						@foreach($funfacts as $funfact)
							<tr>
								<td>
									{{$funfact->title}}
								</td>
								<td>
									{{$funfact->level->name}}
								</td>
								<td class="table-btn-column">
									
									<a class="btn btn-sm btn-default"
									   href="{{action('Admin\FunfactController@edit',$funfact->id)}}">
										<i class="fa fa-wrench"></i> Edit
									</a>
									
									
									<a href="{{$funfact->id}}" data-itemname="{{$funfact->name}}"
									   class="btn btn-sm btn-danger btn-delete">
										<i class="fa fa-trash"></i> Delete
									</a>
								
								
								</td>
							</tr>
						@endforeach
						</tbody>
					</table>
					{!! $funfacts->render() !!}
				
				</div>
			</div>
		</div>
	
	</div>
@endsection

@section('scripts')
	<script>

		var deleteText  = "Funfact will be deleted after form submition.";
		var deletedText = "Funfact was deleted.";
	
	</script>
	{!! Form::open(array('action' => array('Admin\FunfactController@index'), 'method' => 'delete', 'class'=>'form-delete', 'id'=>'formDelete')) !!}
	{!! csrf_field() !!}
	{!! Form::close() !!}
@endsection
