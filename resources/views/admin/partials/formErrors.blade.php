@if($errors->any())
    <div class="row">
        <div class="col-lg-12">
            <ul class="alert alert-error alert-danger alert-form-errors">
                @foreach($errors->all() as $error)
                    <li style="margin-left: 2%">{{$error}}</li>
                @endforeach
            </ul>
        </div>
    </div>
@endif