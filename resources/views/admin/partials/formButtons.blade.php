@php
    $controller = str_replace('App\\Http\\Controllers\\','', substr(Route::currentRouteAction(), 0, (strpos(Route::currentRouteAction(), '@'))));
    $controllerMethod = str_replace('App\\Http\\Controllers\\','', substr(Route::currentRouteAction(), (strpos(Route::currentRouteAction(), '@') + 1)));
@endphp

<div class="row">
    <div class="col-md-12">
        <div class="btn-group" role="group" aria-label="...">
            <button class="btn btn-success hoverRight" name="update" type="submit"><i class="fa fa-check"></i>
                <span>Save</span>
            </button>
            <a class="btn btn-danger hoverRight" href="{{action($controller.'@index')}}"><i class="fa fa-close"></i>
                <span>Cancel</span></a>
        </div>
    </div>
</div>

<hr>
