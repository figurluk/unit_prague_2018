@extends('layouts.admin')

@section('content')

    <div class="pull-right">
        <a class="btn btn-default" href="{{ action('Admin\UsersController@show', $game->user_id) }}">Back</a>
    </div>

    <div class="row">

        <div class="col-lg-6">

            <div class="box">
                <div class="box-header with-border">
                    General
                </div>
                <div class="box-body">
                    <div class="row">
                        <label class="col-sm-4 control-label">User</label>
                        <div class="col-sm-8">
                            {{ $game->user->email }}
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-4 control-label">Token</label>
                        <div class="col-sm-8">
                            {{ $game->token }}
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="row">

        <div class="col-lg-6">

            <div class="box">
                <div class="box-header with-border">
                    Statistics
                </div>
                <div class="box-body">
                    <div class="row">
                        <label class="col-sm-4 control-label">Points</label>
                        <div class="col-sm-8">
                            {{ $game->points }}
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-4 control-label">Total time</label>
                        <div class="col-sm-8">
                            {{ $game->time . ' seconds' }}
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-4 control-label">Rank</label>
                        <div class="col-sm-8">
                            {{ $game->rank }}
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="row">

        <div class="col-lg-12">
            <div class="box">
                <div class="box-header simple">
                    Answers
                </div>
                <div class="box-body">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Level</th>
                            <th>Question</th>
                            <th>Answer</th>
                            <th>Time spent (s)</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($game->answers as $answer)
                            <tr>
                                <td>
                                    {{ $loop->iteration }}
                                </td>
                                <td>{{ $answer->question->level->name }}</td>
                                <td>{{ $answer->question->content }}</td>
                                <td><span class="answer {{ $answer->correct ? '' : 'answer--incorrect' }}">{{ $answer->option->content }}</span></td>
                                <td>{{ $answer->time }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>

@endsection

@section('scripts')

@endsection
