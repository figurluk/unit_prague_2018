@extends('layouts.admin')

@section('content')


    <div class="row">
        <div class="col-lg-12">
            <div class="box">
                <div class="box-header simple">
                    All users
                    <div class="pull-right">
                        <a href="{{action('Admin\UsersController@create')}}"
                           class="btn btn-sm btn-default"><i class="fa fa-plus"></i> Create</a>
                    </div>
                </div>
                <div class="box-body">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>
                                <p>Full name</p>
                            </th>
                            <th>Email</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($users as $user)
                            <tr>
                                <td>
                                    {{$user->name}} {{$user->surname}}
                                </td>
                                <td>
                                    {{$user->email}}
                                </td>
                                <td class="table-btn-column">

                                    <a href="{{ action('Admin\UsersController@show', $user->id) }}"
                                       class="btn btn-sm btn-primary">
                                        Show
                                    </a>

                                    <a class="btn btn-sm btn-default"
                                       href="{{action('Admin\UsersController@edit',$user->id)}}">
                                        <i class="fa fa-wrench"></i> Edit
                                    </a>

                                    <a href="{{$user->id}}" data-itemname="{{$user->name}}"
                                       class="btn btn-sm btn-danger btn-delete">
                                        <i class="fa fa-trash"></i> Delete
                                    </a>

                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {!! $users->render() !!}

                </div>
            </div>
        </div>

    </div>
@endsection

@section('scripts')
    <script>

        var deleteText = "Potvrdením vymažete uživateľa: ";
        var deletedText = "Užívateľ bol úspešne zmazaný.";

    </script>
    {!! Form::open(array('action' => array('Admin\UsersController@index'), 'method' => 'delete', 'class'=>'form-delete', 'id'=>'formDelete')) !!}
    {!! csrf_field() !!}
    {!! Form::close() !!}
@endsection
