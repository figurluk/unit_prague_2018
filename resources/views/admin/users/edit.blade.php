@extends('layouts.admin')

@section('content')

    {!! Form::open(['action'=>['Admin\UsersController@update', $user->id], 'method'=>'PATCH','class'=>'form-horizontal']) !!}


    @include('admin.partials.formButtons')


    <div class="row">
        @include('admin.partials.formErrors')

        <div class="col-lg-6">

            <div class="box">
                <div class="box-header simple">
                    General
                </div>
                <div class="box-body">
                    <div class="form-group">
                        {!! Form::hidden('old_email', $user->email) !!}
                        {!! Form::label('name', 'Name:', ['class'=>'col-sm-4 control-label']) !!}
                        <div class="col-sm-8">
                            {!! Form::text('name', $user->name, ['class'=>'form-control','placeholder'=>'Name']) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        {!! Form::label('email', 'E-mail:', ['class'=>'col-sm-4 control-label']) !!}
                        <div class="col-sm-8">
                            {!! Form::email('email', $user->email, ['class'=>'form-control','placeholder'=>'E-mail']) !!}
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div class="col-lg-6">
            <div class="box">
                <div class="box-header simple">
                    Password
                </div>
                <div class="box-body">
                    <div class="form-group">
                        {!! Form::label('password', 'New password:', ['class'=>'col-sm-4 control-label']) !!}
                        <div class="col-sm-8">
                            {!! Form::password('password', ['class'=>'form-control','placeholder'=>'']) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        {!! Form::label('password_confirmation', 'Confirm password:', ['class'=>'col-sm-4 control-label']) !!}
                        <div class="col-sm-8">
                            {!! Form::password('password_confirmation', ['class'=>'form-control','placeholder'=>'']) !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
@endsection

@section('scripts')

@endsection
