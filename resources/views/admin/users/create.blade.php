@extends('layouts.admin')


@section('content')
    {!! Form::open(['action'=>'Admin\UsersController@store', 'method'=>'POST','class'=>'form-horizontal']) !!}


    @include('admin.partials.formButtons')


    <div class="row">
        @include('admin.partials.formErrors')

        <div class="col-lg-7">
            <div class="box">
                <div class="box-header simple">
                    Všeobecné nastavenia
                </div>
                <div class="box-body">
                    <div class="form-group">
                        {!! Form::label('name', 'Meno:', ['class'=>'col-sm-4 control-label']) !!}
                        <div class="col-sm-8">
                            {!! Form::text('name', null, ['class'=>'form-control','placeholder'=>'Meno a priezvisko']) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        {!! Form::label('email', 'E-mail:', ['class'=>'col-sm-4 control-label']) !!}
                        <div class="col-sm-8">
                            {!! Form::email('email', null, ['class'=>'form-control','placeholder'=>'E-mail']) !!}
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div class="col-lg-5">
            <div class="box">
                <div class="box-header simple">
                    Heslo
                </div>
                <div class="box-body">
                    <div class="form-group">
                        {!! Form::label('password', 'Heslo:', ['class'=>'col-sm-4 control-label']) !!}
                        <div class="col-sm-8">
                            {!! Form::password('password', ['class'=>'form-control','placeholder'=>'Heslo']) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        {!! Form::label('password_confirmation', 'Heslo znova:', ['class'=>'col-sm-4 control-label']) !!}
                        <div class="col-sm-8">
                            {!! Form::password('password_confirmation', ['class'=>'form-control','placeholder'=>'Heslo znova']) !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    {!! Form::close() !!}
@endsection

@section('scripts')

@endsection
