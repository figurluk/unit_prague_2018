@extends('layouts.admin')

@section('content')

    <div class="pull-right">
        <a class="btn btn-default" href="{{ action('Admin\UsersController@index') }}">Back</a>
    </div>


    <div class="row">

        <div class="col-lg-6">

            <div class="box">
                <div class="box-header with-border">
                    General
                </div>
                <div class="box-body">
                    <div class="row">
                        <label class="col-sm-4 control-label">Name</label>
                        <div class="col-sm-8">
                            {{ $user->name }}
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-4 control-label">E-Mail</label>
                        <div class="col-sm-8">
                            {{ $user->email }}
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="row">

        <div class="col-lg-12">
            <div class="box">
                <div class="box-header simple">
                    Games
                </div>
                <div class="box-body">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Date</th>
                            <th>Progress</th>
                            <th>Time spent (s)</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($user->games as $game)
                            <tr>
                                <td>
                                    {{ $loop->iteration }}
                                </td>
                                <td>{{ $game->created_at->format(config('formats.timestamp.web')) }}</td>
                                <td>{{ $game->progress() . '%' }}</td>
                                <td>{{ $game->time }}</td>
                                <td class="table-btn-column">
                                    <a href="{{ action('Admin\GameController@show', $game->id) }}">Detail</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>

@endsection

@section('scripts')

@endsection
