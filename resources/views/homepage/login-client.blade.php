@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="row" id="email-login-form">
			<div class="col-md-6 col-md-offset-3 col-sm-12">

				<h1 class="text-center">Vstup do hry</h1>
				<p class="text-center">Vyplň své údaje a začni hrát</p>
				<form action="{{action('Auth\RegisterController@register')}}" method="POST">
					
					@csrf
					
					<div class="form-group row">
						<label for="name" class="col-sm-4 col-form-label text-md-right">Jméno</label>
						
						<div class="col-md-8">
							<input id="name" type="text"
								   class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name"
								   value="{{ old('name') }}" required autofocus>
							
							@if ($errors->has('name'))
								<div class="alert alert-danger">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </div>
							@endif
						</div>
					</div>
					
					<div class="form-group row">
						<label for="email" class="col-sm-4 col-form-label text-md-right">Email</label>
						
						<div class="col-md-8">
							<input id="email" type="email"
								   class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email"
								   value="{{ old('email') }}" required autofocus>
							
							@if ($errors->has('email'))
								<div class="alert alert-danger">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </div>
							@endif
						</div>
					</div>
					
					<div class="form-group row">
						<div class="col-md-12">
							<button type="submit" class="btn btn-block btn-primary">
								Přihlásit se a začít hrát
							</button>
						</div>
					</div>
				
				</form>
			</div>
		</div>
		<h3 class="title-delimiter"><span>nebo</span></h3>
		<div class="row">
			<div class="col-md-6 col-md-offset-3 col-sm-12">
				<a href="{{action('Auth\LoginController@redirectToFacebookProvider')}}"
				   class="btn btn-block btn-social btn-facebook btn-center-text">
					<span class="fa fa-facebook"></span> Přihlásit přes Facebook
				</a>
				<a href="{{action('Auth\LoginController@redirectToGoogleProvider')}}"
				   class="btn btn-block btn-social btn-google btn-center-text">
					<span class="fa fa-google"></span> Přihlásit přes Google+
				</a>
			</div>
		</div>
	</div>
@endsection
