@php
            $controller = str_replace('App\\Http\\Controllers\\','', substr(Route::currentRouteAction(), 0, (strpos(Route::currentRouteAction(), '@'))));
            @endphp

<div id="flashMessages">
    @if(Session::has('flash_notification.message'))
        <div class="flashMessage infoMessage alert alert-gold" data-laravel="1">
            @if(Session::has('flash_notification.overlay'))
                @if(Session::get('flash_notification.title')=="login")
                    <i class="fa fa-sign-in fa-4x" aria-hidden="true"></i><br>
                @elseif(Session::get('flash_notification.title')=="logout")
                    <i class="fa fa-sign-out fa-4x" aria-hidden="true"></i><br>
                @elseif(Session::get('flash_notification.title')=="register")
                    <i class="fa fa-user fa-4x" aria-hidden="true"></i><br>
                @endif
            @else
                <i class="fa fa-check fa-4x" aria-hidden="true"></i><br>
            @endif
            {{ Session::get('flash_notification.message') }}
        </div>
    @endif
</div>
