<!DOCTYPE html>
<html lang="sk">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="csrf-token" content="<?= csrf_token() ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	<meta name="description" content="Admin Tool for eshop: {{config('app.name')}}">
	<meta name="author" content="E-zone technologies">
	<link rel="icon" href="{{asset('assets-admin/favicon.ico')}}">
	
	<title>Admin Tool | {{config('app.name')}}</title>
	
	<!-- CSS -->
	<link href="https://fonts.googleapis.com/css?family=Comfortaa%7CLato" rel="stylesheet" type="text/css">
	<link href="{{asset('css/admin.css')}}" rel="stylesheet" type="text/css">

@yield('styles')

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>

<body class="skin-blue sidebar-mini">

<div class="wrapper">
	
	<header class="main-header">
		
		<!-- Logo -->
		<a href="{{ action('Admin\AdminController@index') }}" class="logo">
			<!-- mini logo for sidebar mini 50x50 pixels -->
			<span class="logo-mini"><b>A</b>.</span>
			<!-- logo for regular state and mobile devices -->
			<span class="logo-lg"><b>Admin</b>.</span>
		</a>
		
		<!-- Header Navbar: style can be found in header.less -->
		<nav class="navbar navbar-static-top">
			<!-- Sidebar toggle button-->
			<a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
				<span class="sr-only">Toggle navigation</span>
			</a>
			<!-- Navbar Right Menu -->
			<div class="navbar-custom-menu">
				<ul class="nav navbar-nav">
					<!-- Messages: style can be found in dropdown.less-->
					<!-- User Account: style can be found in dropdown.less -->
					<li class="dropdown user user-menu">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<img src="{{asset('img/user2-160x160.jpg')}}" class="user-image" alt="User Image">
							<span class="hidden-xs">{{Auth::user()->name}} {{Auth::user()->surname}}</span>
						</a>
						<ul class="dropdown-menu">
							<!-- Menu Footer-->
							<li class="user-footer">
								<div class="pull-right">
									<a href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"
									   class="btn btn-default btn-flat">Sign out</a>
								</div>
								<form id="logout-form" action="{{ route('logout') }}" method="POST"
									  style="display: none;">
									@csrf
								</form>
							</li>
						</ul>
					</li>
				</ul>
			</div>
		
		</nav>
	</header>
	
	@php
		$controller = str_replace('App\\Http\\Controllers\\','', substr(Route::currentRouteAction(), 0, (strpos(Route::currentRouteAction(), '@'))));
		$controllerMethod = str_replace('App\\Http\\Controllers\\','', substr(Route::currentRouteAction(), (strpos(Route::currentRouteAction(), '@') + 1)));
	@endphp
	
	<aside class="main-sidebar">
		<section class="sidebar" style="height: auto;">
			<!-- Sidebar user panel -->
			<div class="user-panel">
				<div class="pull-left image">
					<img src="{{asset('img/user2-160x160.jpg')}}" class="img-circle" alt="User Image">
				</div>
				<div class="pull-left info">
					<p>{{Auth::user()->name}} {{Auth::user()->surname}}</p>
					<a href="#"><i class="fa fa-circle text-success"></i> Online</a>
				</div>
			</div>
			<!-- sidebar menu: : style can be found in sidebar.less -->
			<ul class="sidebar-menu tree" data-widget="tree">
				<li class="header">MAIN NAVIGATION</li>
				<li {{($controller=="Admin\AdminController" && ($controllerMethod=='homePage')) ? "class=active" : ""}}>
					<a href="{{action('Admin\AdminController@index')}}"><i class="fa fa-fw fa-home"></i>
						<span>Home</span></a>
				</li>
				<li {{($controller=="Admin\UsersController") ? "class=active" : ""}}><a
							href="{{action('Admin\UsersController@index')}}"><i class="fa fa-fw fa-user"></i> <span>Users</span></a>
				</li>
				<li {{($controller=="Admin\RankController") ? "class=active" : ""}}><a
							href="{{action('Admin\RankController@index')}}"><i class="fa fa-fw fa-question"></i>
						<span>Ranking</span></a>
				</li>
				<li {{($controller=="Admin\QuestionController") ? "class=active" : ""}}><a
							href="{{action('Admin\QuestionController@index')}}"><i class="fa fa-fw fa-question"></i>
						<span>Questions</span></a>
				</li>
				<li {{($controller=="Admin\FunfactController") ? "class=active" : ""}}><a
							href="{{action('Admin\FunfactController@index')}}"><i class="fa fa-fw fa-info"></i> <span>Fun facts</span></a>
				</li>
			</ul>
		</section>
	</aside>
	
	<div class="content-wrapper">
		@include('flashes.admin_flash')
		
		<section class="content">
			@yield('content')
		</section>
	</div>
	
	<footer class="main-footer">
		Prevádzkováteľom tohto systému je spoločnosť <a href="http://skoda-auto.cz" target="_blank">ŠKODA AUTO a.s.</a>
	</footer>

</div>

<script src="{{asset('js/admin.js')}}"></script>

<script>
	$.ajaxSetup({headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'}});

	$(document).on('submit', 'form', function (e) {
		$('#page-loader-wrapper').removeClass('hidden');
	});

	$(document).ajaxStart(function (e) {
		$('#page-loader-wrapper').removeClass('hidden');
	});

	$(document).ajaxStop(function (e) {
		$('#page-loader-wrapper').addClass('hidden');
	});

	$(function () {
		$('[data-toggle="tooltip"]').tooltip()
	});

	setTimeout(function () {
		$('#flashesDiv').fadeOut('fast');
	}, 3000);


	$('.btn-delete').click(function (e) {
		e.preventDefault();
		e.stopPropagation();

		var target = $(e.target);

		if (!$(target).hasClass('.btn-delete')) {
			target = $(target).closest('.btn-delete')[0];
		}

		var id = $(target).attr('href');

		swal({
				title             : "Sure delete?",
				text              : deleteText + ($(target).attr('data-itemname') || ''),
				type              : "warning",
				showCancelButton  : true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText : "Yes, delete!",
				cancelButtonText  : "No, close.",
				closeOnConfirm    : false,
				closeOnCancel     : false
			},
			function (isConfirm) {
				if (isConfirm) {
					$("#formDelete").attr('action', $("#formDelete").attr('action') + '/' + id);

					$("#formDelete").submit();
				} else {
					swal("Canceled!", "Nothing deleted. :)", "error");
				}
			});
	});

	setTimeout(function () {
		$('#flashMessage').fadeOut('fast');
	}, 3000);

</script>

@yield('scripts')

</body>
</html>
