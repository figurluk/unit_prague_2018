<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
	<title>{{ config('app.name', 'Laravel') }}</title>
	
	<!-- Fonts -->
	<link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css">
	
	<link rel="manifest" href="{{asset('manifest.json')}}">
</head>
<body id="landing">

<div class="manhattan">
	<div class="m-b m-size-x-2 m-offset-x-6  m-size-y-4 m-color-5"></div>
	<div class="m-b m-size-x-4 m-offset-x-9  m-size-y-8 m-color-4"></div>
	<div class="m-b m-size-x-2 m-offset-x-8  m-size-y-4 m-color-3"></div>
	<div class="m-b m-size-x-2 m-offset-x-15  m-size-y-8 m-color-4"></div>
	<div class="m-b m-size-x-4 m-offset-x-19  m-size-y-18 m-color-1"></div>
	<div class="m-b m-size-x-5 m-offset-x-26  m-size-y-16 m-color-2"></div>
	<div class="m-b m-size-x-10 m-offset-x-17  m-size-y-12 m-color-3"></div>
	<div class="m-b m-size-x-4 m-offset-x-30  m-size-y-8 m-color-4"></div>
	<div class="m-b m-size-x-4 m-offset-x-34  m-size-y-10 m-color-3"></div>
	<div class="m-b m-size-x-6 m-offset-x-38  m-size-y-8 m-color-5"></div>
	<div class="m-b m-size-x-3 m-offset-x-44  m-size-y-12 m-color-5"></div>
	<div class="m-b m-size-x-2 m-offset-x-46  m-size-y-10 m-color-4"></div>
	<div class="m-b m-size-x-4 m-offset-x-48  m-size-y-8 m-color-3"></div>
	<div class="m-b m-size-x-4 m-offset-x-52  m-size-y-16 m-color-2"></div>
	<div class="m-b m-size-x-4 m-offset-x-54  m-size-y-6 m-color-1"></div>
	<div class="m-b m-size-x-4 m-offset-x-58  m-size-y-8 m-color-3"></div>
	<div class="m-b m-size-x-6 m-offset-x-62  m-size-y-12 m-color-2"></div>
	<div class="m-b m-size-x-5 m-offset-x-65  m-size-y-4 m-color-4"></div>
	<div class="m-b m-size-x-2 m-offset-x-70  m-size-y-6 m-color-5"></div>
	<div class="m-b m-size-x-2 m-offset-x-72  m-size-y-6 m-color-3"></div>
</div>


<div class="ground">
	
	
	<div class="road">
		<div class="line"></div>
		<div class="line middle animate">
			<div class="chunk"></div>
			<div class="chunk"></div>
			<div class="chunk"></div>
			<div class="chunk"></div>
			<div class="chunk"></div>
			<div class="chunk"></div>
			<div class="chunk"></div>
			<div class="chunk"></div>
			<div class="chunk"></div>
			<div class="chunk"></div>
			<div class="chunk"></div>
		</div>
		<div class="line"></div>
	</div>

</div>

<div class="billboard">
	<div class="inner">
		<img src="{{ asset('img/skoda-logotype.png') }}"/>
		<h1>Výroba áut</h1>
	</div>
</div>

<a id="start-button" href="{{action('GameController@index')}}" class="btn btn-primary btn-lg">
	Začít hru
</a>

<p id="footer-left">&copy 2018 ŠKODA AUTO</p>
<p id="footer-right">Magic by Fabulous</p>

</body>
</html>
