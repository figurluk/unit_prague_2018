<div id="level5" class="level">
    
    @php
        $level =\App\Models\Level::orderBy('id')->offset(4)->first();
    @endphp

    <div class="content">
        <div class="frame">
            <div class="billboard">
                <div class="inner">
                    <h1>Výroba áut</h1>
                </div>
            </div>

            <div class="text">
                <div class="inner">
                    <p>
                        {{$level->name}}
                    </p>
                </div>
            </div>
        </div>

        <div class="frame">
            <div class="video">
                <iframe width="420" height="315"
                        src="https://www.youtube.com/embed/pag4dSBbqUc">
                </iframe>
            </div>
        </div>
        <div class="frame">
            <div class="funfact">
                <div class="inner">
                    <h2>...vědel si, že?!</h2>
                    <p>{{$level->funfacts()->inRandomOrder()->first()->content}}</p>
                </div>
            </div>
        </div>
        <div class="frame">
            <div>
                <button class="btn btn-lg btn-default">
                    <span>Spustit test</span></button>
            </div>

            <div class="test hidden">

                <div class="inner">
    
    
                    @php
                        $question = $level->questions()->inRandomOrder()->first();
                    @endphp
    
                    <h2>{{$question->content}}</h2>
                    <form>
                        <div class="options">
            
                            @foreach($question->options as $option)
                                <label for="">
                                    <input type="radio" name="option1" value="{{$option->id}}">
                                    {{$option->content}}
                                </label>
                            @endforeach
        
                        </div>
                        <input type="hidden" name="question_id" value="{{$question->id}}">
                        <button class="btn btn-primary btn-lg">Odeslat</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="ground"></div>
</div>

