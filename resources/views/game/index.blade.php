<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css">

</head>
<body id="game">
<div id="game-wrapper">


    @include('game.map')

    <div id="levels">

        <a id="levels-pause" href="#pause"><i class="fa fa-pause-circle-o" aria-hidden="true"></i></a>
        <a id="levels-mute" href="#pause">
            {{--<i class="fa fa-volume-off" aria-hidden="true"></i>--}}
            <i class="fa fa-volume-up" aria-hidden="true"></i>
        </a>

        @include('game.level1')
        @include('game.level2')
        @include('game.level3')
        @include('game.level4')
        @include('game.level5')
    </div>

    <div id="results" class="hidden">
        <div>
            <h1>Porovnej si své skóre!</h1>
        </div>
        <div id="loader">
            <i class="fa fa-spinner fa-spin"></i>
        </div>

    </div>

    <div id="ground"></div>

    <div id="logo">
        <img src="{{asset('img/skoda-logotype.png')}}"/>
    </div>

    <div id="player" class="player standing-right"></div>
</div>
<script
        src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>
<script src="js/game.js"></script>
<script src="answer.js"></script>
<script src="storage.js"></script>

</body>
</html>
