<div id="map">
	
	<div id="map-inner">
		
		
			<h1>Nová hra</h1>
		
		<h2>Urovně</h2>
		<div id="map-levels">
			@php $levels = 5; @endphp
			@for($i = 1; $i <= $levels; $i++)
				
				<a href="#{{ $i }}" class="disabled"><span>{{ $i }}</span></a>
			@endfor
		
		
		</div>
	</div>
</div>
