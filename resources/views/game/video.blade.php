<div id="ytplayer"></div>

<script>
    // Load the IFrame Player API code asynchronously.
    var tag = document.createElement('script');
    tag.src = "https://www.youtube.com/player_api";
    var firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

    var videoId = 'XDzN2wvrssU';
    var startTime = 30;
    var endTime = 40;

    // Replace the 'ytplayer' element with an <iframe> and
    // YouTube player after the API code downloads.
    var player;
    function onYouTubePlayerAPIReady() {
        player = new YT.Player('ytplayer', {
            height: '360',
            width: '640',
            videoId: videoId,
            playerVars: {
                rel: 0,
                autoplay: 0,        // Auto-play the video on load
                controls: 0,        // Show pause/play buttons in player
                showinfo: false,        // Hide the video title
                modestbranding: true,  // Hide the Youtube Logo
                fs: 1,              // Hide the full screen button
                cc_load_policy: 0, // Hide closed captions
                iv_load_policy: 3,  // Hide the Video Annotations
                start: startTime,
                end: endTime,
                loop: 0,            // Run the video in a loop
                autohide: 0         // Hide video controls when playing
            },
        });
    }
</script>

