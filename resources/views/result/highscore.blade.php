<div class="inner">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="text-center ribbon-title">Porovnej si své skóre!</h2>
            </div>
            @foreach($gamesSorted as $game)
                @if ($loop->iteration > 3)
                    @break
                @endif
                <div class="col-sm-4">
                    <div class="ribbon">
                        <h1>{{ $loop->iteration }}. místo</h1>
                        <h4 class="text-center">{{ $game->user->name }}</h4>
                        <h5 class="text-center">{{ $game->points }}</h5>
                    </div>
                </div>
            @endforeach
        </div>
        @if ($gamesSorted->count() > 3)
            <div class="row">
                <div class="col-sm-12">
                    @foreach($gamesSorted->slice(2) as $index => $game)
                        @if ($loop->iteration > 3)
                            @break
                        @endif
                        <div class="panel result-table">
                            <div class="panel-body panel-default">
                                <div class="row">
                                    <div class="col-xs-1 text-center">#{{ $loop->iteration }}</div>
                                    <div class="col-xs-6">{{ $game->user->name }}</div>
                                    <div class="col-xs-4 text-right"><i class="fa fa-clock"></i> {{ $game->points }}
                                        ({{ $game->time }} sekund)
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        @endif
    </div>
</div>
