let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
	.less('resources/assets/less/app.less', 'public/css')
	.options({
		processCssUrls: false
	});

mix.styles([
	'resources/assets/css/*',
	'public/css/font-awesome.css',
	'public/css/app.css'
], 'public/css/app.css');


mix.less('resources/assets/less/bootstrap/bootstrap.less', 'public/css');

mix.less('resources/assets/less/font-awesome/font-awesome.less', 'public/css');

mix.less('resources/assets/less/admin.less', 'public/css');

mix.styles([
	'public/css/bootstrap.css',
	'public/css/font-awesome.css',
	'resources/assets/admin-css/sweetalert.css',
	'node_modules/admin-lte/dist/css/skins/skin-blue.css',
	'node_modules/admin-lte/dist/css/AdminLTE.css',
	'public/css/admin.css'
], 'public/css/admin.css');

mix.js([
	'resources/assets/js/admin.js',
	'resources/assets/js/sweetalert.min.js',
	'node_modules/admin-lte/dist/js/adminlte.js',
], 'public/js/admin.js');
